import { combineReducers } from 'redux';
import sw from './sw';

export const reducers = combineReducers({
    sw
});
