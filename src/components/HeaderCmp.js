'use strict';

import React, { Component, Fragment } from 'react';
import { Navbar, NavItem, Nav} from 'react-bootstrap';
import {Link} from "react-router-dom";

class HeaderCmp extends Component{

    render() {

        return(
            <Fragment>
                <div className="logo-first">
                </div>
                <Navbar>
                    <Nav>
                        <Link to={`/`} className="navbar-item">
                            <span>Search</span>
                        </Link>

                        <Link to={`/history`} className="navbar-item">
                            <span>History</span>
                        </Link>
                    </Nav>
                </Navbar>
            </Fragment>




        );
    }
}


export default HeaderCmp;

