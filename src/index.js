import React from 'react';
import './index.css';
import Root from './Router';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import {reducers} from './reducers';
import { createLogger } from 'redux-logger';
import { applyMiddleware, createStore, compose } from 'redux';
import './css/styles.css';
import { history } from './history';

const store = compose(
    applyMiddleware(thunk, createLogger()),
)(createStore)(reducers, {});


ReactDOM.render(
    <Root store={store} history={history}/>, document.getElementById('root')
);
