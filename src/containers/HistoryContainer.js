import { Component } from 'react';
import { connect } from 'react-redux';
import {getHistory, getSwPersons} from '../reducers/sw';
import {withRouter} from "react-router";
import React from "react";
import LayoutCmp from '../components/LayoutCmp';
import HistoryPersonList from "../components/history/HistoryPersonList";

class HistoryPageContainer extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        const { history } = this.props;

        return(
            <LayoutCmp showFooter={true} showHeader={true}>
                <div className="App">
                    <section className="Wrapper">
                        {Object.keys(history).length > 0 ?
                            <HistoryPersonList persons={history}/>
                            : <div className="no-history">NO Items Found</div>
                        }
                    </section>
                </div>
            </LayoutCmp>
        )
    }
}

function mapStateToProps(state) {
    return {
        history : getHistory(state)
    }
}


export default withRouter(connect(mapStateToProps, null)(HistoryPageContainer));
