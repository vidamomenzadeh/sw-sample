export const QUERY_SEARCH_ALL_PERSONS = (name) => (`{
	  allPersons(filter:{name_starts_with:"${name}"}){
	   name
	   id
	  }
	}`);

export const QUERY_PERSON = (id) => (`{
  Person(id:"${id}"){
    name
    id
    birthYear
    eyeColor
    gender
    hairColor
    height
    films(orderBy: episodeId_ASC){
        episodeId
        id      	
        title
    }
    skinColor
  }
}`);

export const QUERY_CONSECUTIVE  = (episodeId) => (`{
      allPersons(filter:{films_some:{episodeId : ${episodeId}} AND : {films_some:{episodeId : ${episodeId + 1}}} }){
        name
        id
      }
    }`
);
