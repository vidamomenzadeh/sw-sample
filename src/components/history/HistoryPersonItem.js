import React, { Component } from 'react';
import {Link} from "react-router-dom";
import {Col} from "react-bootstrap";

export default class HistoryPersonItem extends Component{

    constructor(props) {
        super(props);
    }

    render(){
        const { person } = this.props;

        return(
            <Col xs={12} md={3} className="search-person-box">
                <Link to={`/person/${person.id}`}>
                    <span>{person.name}</span>
                </Link>
            </Col>
        );
    }
}
