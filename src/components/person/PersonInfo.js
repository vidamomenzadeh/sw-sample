import React, {Component, Fragment} from 'react';
import {Col, Grid, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

export default class PersonInfo extends Component{
    render() {
        const { swPersonInfo , swConsecutivePerson} = this.props;

        return (
            <Fragment >
                <Grid>
                    <Row className="show-grid">
                        <Col className="personal-info-wrapper" xs={12} md={6} >
                            <div>
                                <label>Name:&nbsp;</label>
                                <span>{swPersonInfo.name}</span>
                            </div>
                            <div>
                                <label>BirthYear:&nbsp;</label>
                                <span>{swPersonInfo.birthYear}</span>
                            </div>
                            <div>
                                <label>EyeColor:&nbsp;</label>
                                <span>{swPersonInfo.eyeColor}</span>
                            </div>
                            <div>
                                <label>Gender:&nbsp;</label>
                                <span>{swPersonInfo.gender}</span>
                            </div>
                            <div>
                                <label>HairColor:&nbsp;</label>
                                <span>{swPersonInfo.hairColor}</span>
                            </div>
                            <div>
                                <label>Height:&nbsp;</label>
                                <span>{swPersonInfo.height}</span>
                            </div>
                            <div>
                                <label>SkinColor:&nbsp;</label>
                                <span>{swPersonInfo.skinColor}</span>
                            </div>
                        </Col>
                        <Col xs={12} md={6} >
                            <div className="consecutive-person-title">
                                At least two consecutive movies
                            </div>
                            {
                                Object.keys(swConsecutivePerson).map(id => (
                                    <Link className="consecutive-person-item" to={`/person/${swConsecutivePerson[id].id}`}>
                                        {swConsecutivePerson[id].name}
                                    </Link>
                                ))
                            }
                        </Col>
                    </Row>
                </Grid>

            </Fragment>
        );
    }
}
