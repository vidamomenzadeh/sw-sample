import HistoryPersonItem from './HistoryPersonItem';
import React, { Component, Fragment } from 'react';
import {Grid, Row} from "react-bootstrap";

export default class HistoryPersonList extends Component{

    constructor(props) {
        super(props);
    }

    render(){
        const {persons} = this.props;
        return(
            <Grid>
                <Row className="show-grid">
                    {
                        Object.keys(persons).map(id => (
                            <HistoryPersonItem
                                key={id}
                                person={persons[id]}
                            />
                        ))
                    }
                </Row>
            </Grid>
        )
    }
}
