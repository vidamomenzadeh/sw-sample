import { Component } from 'react';
import { connect } from 'react-redux';
import SearchPersonList from '../components/searchPerson/SearchPersonList';
import * as SWAction from '../actions/sw';
import { getSwPersons }  from '../reducers/sw';
import {withRouter} from "react-router";
import React from "react";
import LayoutCmp from '../components/LayoutCmp';

class SearchPageContainer extends Component {

    constructor(props) {
        super(props);

        this.inputSearchChanged = this.inputSearchChanged.bind(this);
    }

    componentDidMount() {
        this.props.getPersonsData("");
    }

    inputSearchChanged (event){
        let name = event.target.value;
        this.props.getPersonsData(name);
    }

    render() {
        const { swPersons } = this.props;

        return(
            <LayoutCmp showFooter={true} showHeader={true}>
                <div className="App">
                    <input onChange={this.inputSearchChanged} className="search-input" placeholder={"Search"}/>
                    <section className="Wrapper">
                        <SearchPersonList persons={swPersons}/>
                    </section>
                </div>
            </LayoutCmp>
        )
    }
}

function mapStateToProps(state) {
    return {
        swPersons : getSwPersons(state)
    }
}

export default withRouter(connect(mapStateToProps, SWAction)(SearchPageContainer));
