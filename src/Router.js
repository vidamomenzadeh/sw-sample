import React, { Fragment } from 'react';
import { Provider } from 'react-redux';
import { Router, IndexRoute, useRouterHistory } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import ApplicationPageContainer from './containers/SearchPageContainer';
import PersonContainer from './containers/PersonPageContainer';
import HistoryPageContainer from './containers/HistoryContainer';


const Root = ({store, history}) => {
    return <Provider store={store}>
        <Router history={history}>
            <Fragment>
                <Switch>
                    <Route exact path='/' component={ApplicationPageContainer} />
                    <Route exact path='/person/:personId' component={PersonContainer} />
                    <Route exact path='/history' component={HistoryPageContainer} />
                </Switch>
            </Fragment>
        </Router>
    </Provider>
};

export default Root;
