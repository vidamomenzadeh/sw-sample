import {QUERY_SEARCH_ALL_PERSONS, QUERY_PERSON, QUERY_CONSECUTIVE} from '../constants/querys';
import {SERVER_URL} from '../constants/config';

let fetchSwPersons = (searchName) => {
    return fetch(SERVER_URL, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query:  QUERY_SEARCH_ALL_PERSONS(searchName)}),
    }).then(response => response.json())
}

let fetchPerson = (id) => {
    return fetch(SERVER_URL, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: QUERY_PERSON(id) }),
    }).then(response => response.json())
}

let fetchConsecutivePersons = (episodeId) => {
    return fetch(SERVER_URL, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query: QUERY_CONSECUTIVE(episodeId)}),
    }).then(response => response.json())
}

export const getPersonsData = (searchName) => dispatch => {
    return fetchSwPersons(searchName).then((res) => {
        dispatch({
            type: "FETCHED_PERSONS",
            payload: {
                swPersons : res.data.allPersons
            }
        });
    });
}

export const getPersonData = (id) => dispatch => {
    return fetchPerson(id).then((res) => {
        dispatch({
            type: "FETCHED_PERSON",
            payload: {
                person : res.data.Person
            }
        });
    });
}

export const getConsecutivePersonData = (episodeId) => dispatch => {
    return fetchConsecutivePersons(episodeId).then((res) => {
        dispatch({
            type: "FETCHED_CONSECUTIVE_PERSON",
            payload: {
                persons : res.data.allPersons
            }
        });
    });

}