import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as SwPersonAction from '../actions/sw';
import { getSwPersonInfo, getSwConsecutivePersonData}  from '../reducers/sw';
import {withRouter} from "react-router";
import LayoutCmp from "../components/LayoutCmp";
import PersonInfo from "../components/person/PersonInfo";

class Person extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const $this = this;
        const {personId} = this.props.match.params;
        this.fetchPersonData(personId);
    }

    componentWillReceiveProps(newProps){
        const {personId} = this.props.match.params;
        const personNewId = newProps.match.params.personId;

        if(personNewId != personId){
            this.fetchPersonData(personNewId);
        }
    }

    fetchPersonData(personId){
        this.props.getPersonData(personId)
        .then((res) => {
                const { swPersonInfo } = this.props;

                // find consecutive players for this person
                for(var i = 0; i < swPersonInfo.films.length - 1; i++){
                    if(swPersonInfo.films[i].episodeId + 1 == swPersonInfo.films[i + 1].episodeId)
                        this.props.getConsecutivePersonData(swPersonInfo.films[i].episodeId);
                }
            }
        );
    }

    render() {
        const { swPersonInfo, swConsecutivePerson } = this.props;
        return (
            <LayoutCmp showFooter={true} showHeader={true}>
                <PersonInfo swPersonInfo={swPersonInfo} swConsecutivePerson={swConsecutivePerson}/>
            </LayoutCmp>
        );
    }
}


function mapStateToProps(state) {
    return {
        swPersonInfo : getSwPersonInfo(state),
        swConsecutivePerson : getSwConsecutivePersonData(state)
    }
}

export default withRouter(connect(mapStateToProps, SwPersonAction)(Person));
