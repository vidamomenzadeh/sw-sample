import React, {Component, Fragment} from 'react';

import Header from './HeaderCmp';


const defaultProps = {
    showFooter : true,
    showHeader : true,
    showLeftSidebar : true
};

class LayoutCmp extends Component {

    constructor(props){
        super(props);
        this.state = {
            showFooter : true,
            showHeader : true,
            showLeftSidebar : true,
            headerProps : {}
        }
    }

    componentDidMount() {
        if(this.props.showHeader){
            document.body.classList.add("header-fixed");
        }
    }

    renderHeader(showHeader, headerProps){
        if(showHeader){
            const props = headerProps || {};
            return  <Header headerProps={props}></Header>
        }else{
            return null;
        }
    }

    renderFooter(showFooter){
        if(showFooter){
            return <footer className="app-footer">Copyright © 2018, All Rights Reserved.</footer>
        }else{
            return null;
        }
    }

    render() {
        const _props = Object.assign({}, defaultProps, this.props);
        const { children, showHeader, showFooter, headerProps } = _props;
        return (
            <div className="app">
                {this.renderHeader(showHeader, headerProps)}

                <div className="app-body">
                    { children }
                </div>

                {this.renderFooter(showFooter)}
            </div>
        );
    }
}

export default LayoutCmp;
