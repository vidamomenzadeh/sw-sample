import { combineReducers } from 'redux';

let initialState = {
    listSwPersons  : [],
    swPerson : {},
    visitedLinks : [],
    consecutivePersons : []
}


export const listSwPersons = (state = initialState.listSwPersons, action)=> {
    switch (action.type) {
        case "FETCHED_PERSONS":
            return{
                ...state,
                ...action.payload.swPersons
            }

        default:
            return state;
    }
};

export const swPerson = (state = initialState.swPerson, action)=> {
    switch (action.type) {
        case "FETCHED_PERSON":
            return{
                ...state,
                ...action.payload.person
            }

        default:
            return state;
    }
};

export const visitedLinks = (state = initialState.visitedLinks, action)=> {
    switch (action.type) {
        case "FETCHED_PERSON":
            return{
                ...state,
                [action.payload.person.id] : action.payload.person
            }

        default:
            return state;
    }
};

export const consecutivePersons = (state = initialState.consecutivePersons, action)=> {
    switch (action.type) {
        case "FETCHED_CONSECUTIVE_PERSON":

            return{
                ...state,
                ...action.payload.persons.reduce((obj, app) => {
                    obj[app.id] = app;
                    return obj;
                }, {})
            }

        default:
            return state;
    }
};

export const getSwPersons = (state) => {
    return  state ? state.sw.listSwPersons : [];
};

export const getSwPersonInfo = (state) => {
    return  state ? state.sw.swPerson : {};
};

export const getHistory = (state) => {
    return  state ? state.sw.visitedLinks : [];
};

export const getSwConsecutivePersonData = (state) => {
    return  state ? state.sw.consecutivePersons : [];
};


export default combineReducers({
    listSwPersons,
    swPerson,
    visitedLinks,
    consecutivePersons
});
